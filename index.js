
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Stanciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(isFinite(value)){
        if(value>Number.MAX_SAFE_INTEGER||value<=Number.MIN_VALUE){//MIN_SAFE_INTEGER este cel mai mic numar (negativ), iar in test se verifica ca input MIN_VALUE -> 0
            return NaN;
        }
        return Math.floor(value);// verifica atat daca val primita este NaN, iar daca este string o transforma in number inainte de a extrage partea intreaga
    }
    else{
        return NaN;
    }

}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

